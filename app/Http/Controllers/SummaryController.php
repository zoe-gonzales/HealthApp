<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Summary;

use Validator;

class SummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diets = Summary::getAllDiet();
        $exercises = Summary::getAllExercise();
        $stressreliefs = Summary::getAllStressRelief();

        $water_total = 0;
        $produce_total = 0;
        $cardio_stretch = 0;
        $lifting_list = [];
        $mindful = 0;
        $connection = 0;
        $mental = 0;

        foreach($diets as $diet) {
            if ($diet->type === 'Water') {
                $water_total = ($water_total + intval($diet->quantity)) * 8;
            } elseif ($diet->type === 'Fruit' || $diet->type === 'Vegetable') {
                $produce_total = $produce_total + intval($diet->quantity);
            }
        }

        foreach($exercises as $exercise) {
            if ($exercise->type === 'Cardio' || $exercise->type === 'Stretch') {
                $cardio_stretch = ($cardio_stretch + intval($exercise->duration));
            } elseif ($exercise->type === 'Weight-lifting') {
                array_push($lifting_list, $exercise);
            }
        }

        foreach($stressreliefs as $relief) {
            if ($relief->type === 'Mediation' || 
                $relief->type === 'Free time' || 
                $relief->type === 'Screen-free') {
                $mindful = ($mindful + intval($relief->duration)) / 60;
                $mindful = round($mindful, 2);
            } elseif ($relief->type === 'Family & Friends' ||
                $relief->type === 'Hobby') {
                $connection = ($connection + intval($relief->duration)) / 60;
                $connection = round($connection, 2);
            } elseif ($relief->type === 'Therapy') {
                $mental = ($mental + intval($relief->duration)) / 60;
                $mental = round($mental, 2);
            }
        }

        return view('summaries.index', compact('water_total', 'produce_total', 'cardio_stretch', 'mindful', 'connection', 'mental', 'lifting_list', 'diets', 'exercises', 'stressreliefs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('summaries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Summary::getDietById($id);

        return view('summaries.edit')
                ->with('summaries', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->get('dataType') === 'diet') {
            updateDiet($request, $id);
        }
    }

    // Update Diet entry
    public function updateDiet($request, $id){
        $validator = Validator::make($request->all(), [
            'item'=>'required',
            'type'=>'required',
            'quantity'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect('summaries.edit')
                    ->withErrors($validator)
                    ->withInput();
        } 

        else {
            $diet = Summary::getDietById($id);
            $diet->item = $request->get('item');
            $diet->type = $request->get('type');
            $diet->quantity = $request->get('quantity');
            $diet->save();
            
            return redirect('summaries')->with('success', 'Entry saved!');
        }
    }
    // Update Exercise entry
    public function updateExercise($request, $id){
        $validator = Validator::make($request->all(), [
            'activity'=>'required',
            'type'=>'required',
            'duration'=>'nullable',
            'reps'=>'nullable',
            'weight'=>'nullable'
        ]);

        if ($validator->fails()) {
            return redirect('summaries.edit')
                    ->withErrors($validator)
                    ->withInput();
        } 

        else {
            $exercise = Summary::getExerciseById($id);
            $exercise->activity = $request->get('activity');
            $exercise->type = $request->get('type');
            $exercise->duration = $request->get('duration');
            $exercise->reps = $request->get('reps');
            $exercise->weight = $request->get('weight');
            $exercise->save();
            
            return redirect('summaries')->with('success', 'Entry saved!');
        }
    }
    // Update Stress Relief entry
    public function updateStressRelief($request, $id){
        $validator = Validator::make($request->all(), [
            'activity'=>'required',
            'type'=>'required',
            'duration'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect('summaries.edit')
                    ->withErrors($validator)
                    ->withInput();
        } 

        else {
            $stress_relief = Summary::getDietById($id);
            $stress_relief->activity = $request->get('activity');
            $stress_relief->type = $request->get('type');
            $stress_relief->duration = $request->get('duration');
            $stress_relief->save();
            
            return redirect('summaries')->with('success', 'Entry saved!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
