<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exercise;

use Validator;

class ExerciseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = Exercise::getAll();
        // return view('exercises.index', compact('data'));
        $data = Exercise::getAll();
        $cardio_count = 0;
        $stretch_count = 0;
        $lifting_list = [];
        
        foreach ($data as $activity) {
            if ($activity->type === 'Cardio') {
                $num = intval($activity->duration);
                $cardio_count = $cardio_count + $num;
            } elseif ($activity->type === 'Stretch') {
                $num = intval($activity->duration);
                $stretch_count = $stretch_count + $num;
            } elseif ($activity->type === 'Weight-lifting') {
                array_push($lifting_list, $activity);
            }
        }

        return view('exercises.index', compact('cardio_count', 'stretch_count', 'lifting_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('exercises.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'activity'=>'required',
            'type'=>'required',
            'duration'=>'nullable',
            'reps'=>'nullable',
            'weight'=>'nullable'
        ]);

        if ($validator->fails()) {
            return redirect('exercises.create')
                    ->withErrors($validator)
                    ->withInput();
        } 

        else {
            Exercise::add([
                'activity' => $request->get('activity'),
                'type' => $request->get('type'),
                'duration' => $request->get('duration'),
                'reps' => $request->get('reps'),
                'weight' => $request->get('weight')
            ]);
            
            return redirect('exercises')->with('success', 'Entry saved!');
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
