<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Stress_Relief;

use Validator;

class StressReliefController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Stress_Relief::getAll();
        $mindful_count = 0;
        $connection_count = 0;
        $mental_count = 0;

        foreach ($data as $activity) {
            if ($activity->type === 'Mediation' || 
                $activity->type === 'Free time' || 
                $activity->type === 'Screen-free') {
                $num = intval($activity->duration);
                $mindful_count = ($mindful_count + $num) / 60;
                $mindful_count = round($mindful_count, 2);
            } elseif ($activity->type === 'Family & Friends' ||
                    $activity->type === 'Hobby') {
                $num = intval($activity->duration);
                $connection_count = ($connection_count + $num) / 60;
                $connection_count = round($connection_count, 2);
            } elseif ($activity->type === 'Therapy') {
                $num = intval($activity->duration);
                $mental_count = ($mental_count + $num) / 60;
                $mental_count = round($mental_count, 2);
            }
        }

        return view('stressreliefs.index', compact('mindful_count', 'connection_count', 'mental_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('stressreliefs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'activity'=>'required',
            'type'=>'required',
            'duration'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect('stressreliefs.create')
                    ->withErrors($validator)
                    ->withInput();
        } 

        else {
            Stress_Relief::add([
                'activity' => $request->get('activity'),
                'type' => $request->get('type'),
                'duration' => $request->get('duration')
            ]);
            
            return redirect('stressreliefs')->with('success', 'Entry saved!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
