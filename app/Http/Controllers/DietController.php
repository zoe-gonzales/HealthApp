<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Diet;

use Validator;

class DietController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Diet::getAll();
        $water_count = 0;
        $fruit_count = 0;
        $veggie_count = 0;
        
        foreach ($data as $item) {
            if ($item->type === 'Water') {
                $num = intval($item->quantity);
                $water_count = ($water_count + $num) * 8;
            } elseif ($item->type === 'Fruit') {
                $num = intval($item->quantity);
                $fruit_count = $fruit_count + $num;
            } elseif ($item->type === 'Vegetable') {
                $num = intval($item->quantity);
                $veggie_count = $veggie_count + $num;
            }
        }

        return view('diets.index', compact('water_count', 'fruit_count', 'veggie_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('diets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'item'=>'required',
            'type'=>'required',
            'quantity'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect('diets.create')
                    ->withErrors($validator)
                    ->withInput();
        } 

        else {
            Diet::add([
                'item' => $request->get('item'),
                'type' => $request->get('type'),
                'quantity' => $request->get('quantity')
            ]);
            
            return redirect('diets')->with('success', 'Entry saved!');
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
