<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Goal extends Model
{
    public static function getAll(){
        $data = DB::table('goals')->orderBy('id', 'asc')->get();
        return $data;
    }

    public static function add($data){
        DB::table('goals')->insert($data);
        return $data;
    }

    protected $fillable = [
        'name',
        'category',
        'description',
        'deadline'
    ];
}
