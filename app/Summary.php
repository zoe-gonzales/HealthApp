<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Summary extends Model
{
    public static function getAllDiet(){
        $data = DB::table('diets')->orderBy('id', 'asc')->get();
        return $data;
    }

    public static function getDietById($id){
        $data = DB::table('diets')->find($id);
        return $data;
    }

    public static function getAllExercise(){
        $data = DB::table('exercises')->orderBy('id', 'asc')->get();
        return $data;
    }

    public static function getExerciseById($id){
        $data = DB::table('exercises')->find($id);
        return $data;
    }

    public static function getAllStressRelief(){
        $data = DB::table('stressreliefs')->orderBy('id', 'asc')->get();
        return $data;
    }

    public static function getStressReliefById($id){
        $data = DB::table('stressreliefs')->find($id);
        return $data;
    }
}
