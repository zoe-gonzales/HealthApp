<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Diet extends Model
{

    public static function getAll(){
        $data = DB::table('diets')->orderBy('id', 'asc')->get();
        return $data;
    }

    public static function add($data){
        DB::table('diets')->insert($data);
        return $data;
    }

    protected $fillable = [
        'item',
        'type',
        'quantity'
    ];
}
