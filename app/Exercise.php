<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Exercise extends Model
{
    public static function getAll(){
        $data=DB::table('exercises')->orderBy('id', 'asc')->get();
        return $data;
    }

    public static function add($data){
        DB::table('exercises')->insert($data);
        return $data;
    }

    protected $fillable = [
        'activity',
        'type',
        'duration',
        'reps',
        'weight'
    ];
}
