<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Stress_Relief extends Model
{
    public static function getAll(){
        $data=DB::table('stressreliefs')->orderBy('id', 'asc')->get();
        return $data;
    }

    public static function add($data){
        DB::table('stressreliefs')->insert($data);
        return $data;
    }

    protected $fillable = [
        'activity',
        'type',
        'duration'
    ];
}
