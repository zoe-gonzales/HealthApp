<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300i&display=swap" rel="stylesheet">
        <!-- Bootswatch -->
        <link rel="stylesheet" href="./css/bootstrap.min.css"/>
        <!-- Custom CSS -->
        <link rel="stylesheet" href="./css/index.css"/>
    </head>
    <body>
        <style>
            body {
                background-color: #3BCCBE;
            }

            .container {
                margin: 1rem;
            }

            .jumbotron {
                background-color: rgba(0, 0, 0, 0);
                color: black;
                border-radius: 0;
            }

            .display-4 {
                margin-top: 2rem;
            }

            .display-4,
            .lead {
                color: #373F40;
            }

            .form-group {
                color: #373F40;
            }
        </style>

        <!-- NavBar -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Wellness App</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/summaries">Weekly Summary</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/goals">Goals</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Daily Stats
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/diets">Diet</a>
                    <a class="dropdown-item" href="/exercises">Exercise</a>
                    <a class="dropdown-item" href="/stressreliefs">Stress Relief</a>
                    </div>
                </li>
                </ul>
            </div>
        </nav>
        
        <div class="container">@yield('content')</div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script>
            $(function () {
                $('[data-toggle="popover"]').popover()
            });

            $(document).on('click', '.dietUpdate', function(){
                $('#diet').modal('show');
                let id = $(this).attr('id');
                let type = $(this).attr('data-type');
                
                console.log(id);
                console.log(type);
                
                $('#diet-form').on('submit', function(event) {
                    event.preventDefault();
                    updateData(type, id);
                });
                
            });

            $(document).on('click', '.exerciseUpdate', function(){
                $('#exercise').modal('show');
                console.log($(this).attr('id'));
                console.log($(this).attr('data-type'));
            });

            $(document).on('click', '.reliefUpdate', function(){
                $('#stress-relief').modal('show');
                console.log($(this).attr('id'));
                console.log($(this).attr('data-type'));
            });

            function updateData(dataType, id){
                console.log(dataType + ' | ' + id);
                let updatedData = {
                    item: $("#diet-form [name=item]").val().trim(),
                    type: $("#diet-form [name=type]").val().trim(),
                    quantity: $("#diet-form [name=quantity]").val().trim(),
                    dataType: dataType,
                    id: id
                }

                $.ajax('/summaries/update', id, {
                    method: 'POST',
                    data: updatedData
                }).then(function(){
                    location.reload();
                });

            }
        </script>
    </body>
</html>
