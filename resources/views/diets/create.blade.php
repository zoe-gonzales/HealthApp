@extends('layout')

@section('title')
Diet
@stop

@section('content')
<div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <a class="nav-link active" id="v-pills-diet-tab" data-toggle="pill" href="#v-pills-diet" role="tab" aria-controls="v-pills-diet" aria-selected="true">Diet Summary</a>
      <a class="nav-link" id="v-pills-add-tab" data-toggle="pill" href="#v-pills-add" role="tab" aria-controls="v-pills-add" aria-selected="false">Add</a>
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-diet" role="tabpanel" aria-labelledby="v-pills-diet-tab">
      <div class="row">
        <!-- Cards -->
        <div class="col-md-4">
          <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
            <div class="card-header">hydration</div>
            <div class="card-body">
              <h5 class="card-title">Total water (oz):</h5>
              <p class="card-text">
                <span class="display-2">32</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
            <div class="card-header">fruit</div>
            <div class="card-body">
              <h5 class="card-title">Total fruit:</h5>
              <p class="card-text">
                <span class="display-2">1</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
            <div class="card-header">vegetables</div>
            <div class="card-body">
              <h5 class="card-title">Total vegetables:</h5>
              <p class="card-text">
                <span class="display-2">6</span>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- End of Cards -->
      </div>
      hello
      <div class="tab-pane fade" id="v-pills-add" role="tabpanel" aria-labelledby="v-pills-add-tab">
        <!-- Form -->
        <p class="lead">
          Add fruits, vegetables, and hydration
        </p>
        <hr>
        <form method="POST" action="{{ route('diets.store') }}">
        @csrf
          <div class="form-group">
            <label for="item">Item</label>
            <input type="text" class="form-control" id="item">
          </div>
          <div class="form-group">
            <label for="type">Select type</label>
            <select class="form-control" id="type">
              <option>Fruit</option>
              <option>Vegetable</option>
              <option>Water</option>
            </select>
          </div>
          <div class="form-group">
            <label for="quantity">Select quantity (1 serving = 8oz of water)</label>
            <input type="text" class="form-control" id="quantity">
          </div>
          <button type="submit" class="btn btn-danger">Add</button>
        </form>
        <!-- End Form -->
      </div>
    </div>
  </div>
</div>
@stop