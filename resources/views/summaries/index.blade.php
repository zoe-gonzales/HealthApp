@extends('layout')

@section('title')
Summary
@stop

@section('content')
<div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <a class="nav-link active" id="v-pills-summary-tab" data-toggle="pill" href="#v-pills-summary" role="tab" aria-controls="v-pills-summary" aria-selected="true">Weekly Summary</a>
      <a class="nav-link" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all" role="tab" aria-controls="v-pills-all" aria-selected="false">All</a>
      <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a>
    </div>
  </div>
  <div class="col-9">
    <!-- Summary Cards -->
    <div class="tab-content" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-summary" role="tabpanel" aria-labelledby="v-pills-summary-tab">
      <!-- Row 1 -->
      <div class="row">
        <div class="col-md-4">
          <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
            <div class="card-header">water</div>
            <div class="card-body">
              <h5 class="card-title">Water total (oz):</h5>
              <p class="card-text">
                <span class="display-2">{{{ $water_total }}}</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
            <div class="card-header">fruits & veggies</div>
            <div class="card-body">
              <h5 class="card-title">Fruits & veggies total:</h5>
              <p class="card-text">
                <span class="display-2">{{{ $produce_total }}}</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-white bg-warning mb-3" style="max-width: 18rem;">
            <div class="card-header">cardio & stretch</div>
            <div class="card-body">
              <h5 class="card-title">Total (min):</h5>
              <p class="card-text">
                <span class="display-2">{{{ $cardio_stretch }}}</span>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- Row 2 -->
      <div class="row">
        <div class="col-md-4">
          <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
            <div class="card-header">mindfulness</div>
            <div class="card-body">
              <h5 class="card-title">Mindfulness total (hrs)</h5>
              <p class="card-text">
                <span class="display-2">{{{ $mindful }}}</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-white bg-secondary mb-3" style="max-width: 18rem;">
            <div class="card-header">face-to-face connection</div>
            <div class="card-body">
              <h5 class="card-title">Connection total (hrs)</h5>
              <p class="card-text">
                <span class="display-2">{{{ $connection }}}</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card bg-light mb-3" style="max-width: 18rem;">
            <div class="card-header">mental health</div>
            <div class="card-body">
              <h5 class="card-title">Mental health (hrs)</h5>
              <p class="card-text">
                <span class="display-2">{{{ $mental }}}</span>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- Row 3 -->
      <div class="row">
        <div class="col-md-12">
        <div class="card text-white bg-success mb-3">
          <div class="card-header">weights</div>
          <div class="card-body">
          @foreach ($lifting_list as $session)
          <div class="entry">
              <h5 class="card-title">Session {{{ $session->id }}}</h5>
              <p class="card-text">
                Reps <br>
                <span class="display-2">{{{ $session->reps }}}</span>
                <br> Weight <br>
                <span class="display-2">{{{ $session->weight }}}</span>
                lbs
              </p>
          </div>
          @endforeach
          </div>
        </div>
      </div>
      </div>
      <!-- End Cards -->
  </div>
    <!-- End Cards -->
    <div class="tab-pane fade" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">
      <div class="card">
        <ul class="list-group list-group-flush">
            <!-- Diet -->
            <li class="list-group-item header">
                <div class="row">
                    <div class="col-3">Item</div>
                    <div class="col-3">Type</div>
                    <div class="col-3">Quantity</div>
                    <div class="col-3"></div>
                </div>
            </li>
            @foreach ($diets as $diet)
            <li class="list-group-item">
                <div class="row">
                    <div class="col-3">{{{ $diet->item }}}</div>
                    <div class="col-3">{{{ $diet->type }}}</div>
                    <div class="col-3">{{{ $diet->quantity }}}</div>
                    <div class="col-3">
                      <button data-type="diet" id={{{ $diet->id }}} type="button" class="btn btn-info btn-sm dietUpdate">Update</button>
                    </div>
                </div>
            </li>
            @endforeach
            <!-- Stress Relief -->
            <li class="list-group-item header">
                <div class="row">
                    <div class="col-3">Activity</div>
                    <div class="col-3">Type</div>
                    <div class="col-3">Duration (min)</div>
                    <div class="col-3"></div>
                </div>
            </li>
            @foreach ($stressreliefs as $relief)
            <li class="list-group-item">
                <div class="row">
                    <div class="col-3">{{{ $relief->activity }}}</div>
                    <div class="col-3">{{{ $relief->type }}}</div>
                    <div class="col-3">{{{ $relief->duration }}}</div>
                    <div class="col-3">
                      <button data-type="stress-relief" id={{{ $relief->id }}} type="button" class="btn btn-warning btn-sm exerciseUpdate">Update</button>
                    </div>
                </div>
            </li>
            @endforeach
            <!-- Exercise -->
            <li class="list-group-item header">
                <div class="row">
                    <div class="col-2">Activity</div>
                    <div class="col-2">Type</div>
                    <div class="col-2">Duration (min)</div>
                    <div class="col-3">Reps/Weight (lbs)</div>
                    <div class="col-3"></div>
                </div>
            </li>
            @foreach ($exercises as $exercise)
            <li class="list-group-item">
                <div class="row">
                    <div class="col-2">{{{ $exercise->activity }}}</div>
                    <div class="col-2">{{{ $exercise->type }}}</div>
                    <div class="col-2">{{{ $exercise->duration }}}</div>
                    <div class="col-3">
                      @if ($exercise->reps && $exercise->weight)
                        {{{ $exercise->reps }}}/{{{ $exercise->weight }}}
                      @endif
                    </div>
                    <div class="col-3">
                      <button data-type="exercise" id={{{ $exercise->id }}} type="button" class="btn btn-success btn-sm reliefUpdate">Update</button>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
      </div>
    </div>
    <!-- Settings -->
    <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">Coming soon</div>
    </div>
  </div>
</div>

<!-- Diet Update Modal -->
<div class="modal" tabindex="-1" role="dialog" id="diet">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">update diet</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="diet-form">
          @csrf
          <div class="form-group">
            <input type="text" class="form-control" id="item" name="item" placeholder="Item name">
          </div>
          <div class="form-group">
            <select class="form-control" id="type" name="type">
              <option>Fruit</option>
              <option>Vegetable</option>
              <option>Water</option>
            </select>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Select quantity (1 serving = 8oz of water)">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End Diet Modal -->

<!-- Exercise Update Modal -->
<div class="modal" tabindex="-1" role="dialog" id="exercise">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">update exercise</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="POST" action="{{ route('stressreliefs.store') }}">
          @csrf
          <div class="form-group">
            <input type="text" class="form-control" id="activity" name="activity" placeholder="Activity">
          </div>
          <div class="form-group">
            <select class="form-control" id="type" name="type">
              <option>Cardio</option>
              <option>Weight-lifting</option>
              <option>Stretch</option>
            </select>
          </div>
          <div class="modal-title">Cardio & Stretch Only</div>
          <!-- Duration for Cardio & Stretch -->
          <div class="form-group">
            <label for="duration"></label>
            <input type="text" class="form-control" id="duration" name="duration" placeholder="Duration (in min)">
          </div>
          <div class="modal-title">Weight-Lifting Only</div>
          <hr>
          <!-- Reps -->
          <div class="form-group">
            <input type="text" class="form-control" id="reps" name="reps" placeholder="# of Reps">
          </div>
          <!-- Weight -->
          <div class="form-group">
            <input type="text" class="form-control" id="weight" name="weight" placeholder="Weight (in lbs)">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
      </form>
      </div>
    </div>
  </div>
</div>
<!-- End Exercise Modal -->

<!-- Stress Relief Modal -->
<div class="modal" tabindex="-1" role="dialog" id="stress-relief">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">update stress relief</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="POST" action="{{ route('stressreliefs.store') }}">
          @csrf
          <div class="form-group">
            <input type="text" class="form-control" id="activity" name="activity" placeholder="Activity">
          </div>
          <div class="form-group">
            <select class="form-control" id="type" name="type">
            <option>Mediation</option>
              <option>Free time</option>
              <option>Screen-free</option>
              <option>Family & Friends</option>
              <option>Hobby</option>
              <option>Therapy</option>
            </select>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="duration" name="duration" placeholder="Duration (in min)">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
      </form>
      </div>
    </div>
  </div>
</div>
<!-- End Stress Relief Modal -->
@stop