@extends('layout')

@section('title')
Home
@stop

@section('content')

<div class="row">
    <!-- Image -->
    <div class="col-md-6">
        <img src="./images/brain-heart.png" class="img-fluid" alt="Responsive image">
    </div>
    <!-- Text -->
    <div class="col-md-6">
        <h1 class="display-4">About</h1>
        <p class="lead">
            No one likes to count calories, so when tasked with creating a health app, I decided to take a different approach. Instead of measuring every single calorie consumed or burned, this app takes a more simplified approach by counting:
            <ul class="lead">
                <li>Servings of water, fruit, and vegetables consumed daily.</li>
                <li>Total minutes of cardio and stretch per week; number of reps and weight (lbs) lifted per session.</li>
                <li>Total hours of mindfulness, face-to-face connection, and mental healthcare per week.</li>
            </ul>

            <p class="lead">Take a look around, and feel free to set your own goals. I hope you enjoy!</p>

            <a class="lead link" target="_blank" href="https://github.com/zoe-gonzales/HealthApp">Made with ❤️ in Denver</a>
        </p>
    </div>
</div>
@stop