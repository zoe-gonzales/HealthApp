@extends('layout')

@section('title')
Goals
@stop

@section('content')
<div class="row">
    <!-- Nav links -->
    <div class="col-3">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link active" id="v-pills-goals-tab" data-toggle="pill" href="#v-pills-goals" role="tab" aria-controls="v-pills-goals" aria-selected="true">Goals</a>
        <a class="nav-link" id="v-pills-add-tab" data-toggle="pill" href="#v-pills-add" role="tab" aria-controls="v-pills-add" aria-selected="false">Add</a>
        </div>
    </div>
    <!-- End Nav links -->
    <div class="col-9">
    <div class="tab-content" id="v-pills-tabContent">
    <!-- Display all goals -->
      <div class="tab-pane fade show active" id="v-pills-goals" role="tabpanel" aria-labelledby="v-pills-goals-tab">
        <div class="card">
            <ul class="list-group list-group-flush">
                <li class="list-group-item header">
                    <div class="row">
                        <div class="col-3">Name</div>
                        <div class="col-2">Category</div>
                        <div class="col-2">Deadline</div>
                        <div class="col-2">Details</div>
                        <div class="col-3">Edit</div>
                    </div>
                </li>
                @foreach ($goals as $goal)
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-3">{{{ $goal->name }}}</div>
                        <div class="col-2">{{{ $goal->category }}}</div>
                        <div class="col-2">{{{ $goal->deadline }}}</div>
                        <div class="col-2">
                            <button type="button" class="btn btn-success btn-sm" data-container="body" data-toggle="popover" data-placement="bottom" data-content="{{{ $goal->description }}}">
                                Details
                            </button>
                        </div>
                        <div class="col-3">
                            <button type="button" class="btn btn-info btn-sm">✓</button>
                            <button type="button" class="btn btn-warning btn-sm">x</button>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
      </div>
    <!-- Add goal form -->
    <div class="tab-pane fade" id="v-pills-add" role="tabpanel" aria-labelledby="v-pills-add-tab">
        <!-- Form Content -->
        <p class="lead">Add goal</p>
        <hr>
        <form method="POST" action="{{ route('goals.store') }}">
            @csrf
            <!-- Goal name -->
            <div class="form-group">
            <label for="name">Goal name</label>
            <input type="text" class="form-control" id="name" name="name">
            </div>
            <!-- Goal category -->
            <div class="form-group">
            <label for="category">Goal category</label>
            <select class="form-control" id="category" name="category">
                <option>Fruits & Veggies</option>
                <option>Hydration</option>
                <option>Cardio</option>
                <option>Stretch</option>
                <option>Weight-lifting</option>
                <option>Mindfulness</option>
                <option>Relationships</option>
                <option>Mental Health</option>
                <option>Hobby</option>
            </select>
            </div>
            <!-- Goal description -->
            <div class="form-group">
            <label for="description">Goal description</label>
            <textarea class="form-control" id="description" rows="3" name="description"></textarea>
            </div>
            <!-- Deadline -->
            <div class="form-group">
            <label for="deadline">Deadline</label>
            <input type="date" class="form-control" id="deadline" name="deadline">
            </div>
            <button type="submit" class="btn btn-danger">Add</button>
        </form>
        </div>
        </div>
    </div>
</div>
@stop