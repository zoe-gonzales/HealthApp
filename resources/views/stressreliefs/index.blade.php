@extends('layout')

@section('title')
Stress Relief
@stop

@section('content')
<div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <a class="nav-link active" id="v-pills-stress-tab" data-toggle="pill" href="#v-pills-stress" role="tab" aria-controls="v-pills-stress" aria-selected="true">Stress Relief Summary</a>
      <a class="nav-link" id="v-pills-add-tab" data-toggle="pill" href="#v-pills-add" role="tab" aria-controls="v-pills-add" aria-selected="false">Add</a>
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-stress" role="tabpanel" aria-labelledby="v-pills-stress-tab">
      <div class="row">
        <!-- Cards -->
        <div class="col-md-4">
          <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
            <div class="card-header">mindfulness & meditation</div>
            <div class="card-body">
              <h5 class="card-title">Mindfulness hours:</h5>
              <p class="card-text">
                <span class="display-2">{{{ $mindful_count }}}</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
            <div class="card-header">face-to-face connection</div>
            <div class="card-body">
              <h5 class="card-title">Connection hours:</h5>
              <p class="card-text">
                <span class="display-2">{{{ $connection_count }}}</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
            <div class="card-header">mental health</div>
            <div class="card-body">
              <h5 class="card-title">Mental health hours:</h5>
              <p class="card-text">
                <span class="display-2">{{{ $mental_count }}}</span>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- End of Cards -->
      </div>
      <div class="tab-pane fade" id="v-pills-add" role="tabpanel" aria-labelledby="v-pills-add-tab">
        <!-- Form -->
        <p class="lead">
          Add activity
        </p>
        <hr>
        <form method="POST" action="{{ route('stressreliefs.store') }}">
        @csrf
          <div class="form-group">
            <label for="activity">Activity</label>
            <input type="text" class="form-control" id="activity" name="activity">
          </div>

          <div class="form-group">
            <label for="type">Select type</label>
            <select class="form-control" id="type" name="type">
            <option>Mediation</option>
              <option>Free time</option>
              <option>Screen-free</option>
              <option>Family & Friends</option>
              <option>Hobby</option>
              <option>Therapy</option>
            </select>
          </div>

          <div class="form-group">
            <label for="duration">Duration (in min)</label>
            <input type="text" class="form-control" id="duration" name="duration">
          </div>
          <button type="submit" class="btn btn-danger">Add</button>
        </form>
        <!-- End Form -->
      </div>
    </div>
  </div>
</div>
@stop