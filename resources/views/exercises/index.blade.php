@extends('layout')

@section('title')
Exercise
@stop

@section('content')
<div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <a class="nav-link active" id="v-pills-exercise-tab" data-toggle="pill" href="#v-pills-exercise" role="tab" aria-controls="v-pills-exercise" aria-selected="true">Exercise Summary</a>
      <a class="nav-link" id="v-pills-add-tab" data-toggle="pill" href="#v-pills-add" role="tab" aria-controls="v-pills-add" aria-selected="false">Add</a>
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-exercise" role="tabpanel" aria-labelledby="v-pills-exercise-tab">
      <div class="row">
        <!-- Cards -->
        <div class="col-md-4">
          <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
            <div class="card-header">cardio</div>
            <div class="card-body">
              <h5 class="card-title">Cardio (min):</h5>
              <p class="card-text">
                <span class="display-2">{{{ $cardio_count }}}</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
            <div class="card-header">stretch</div>
            <div class="card-body">
              <h5 class="card-title">Stretch (min):</h5>
              <p class="card-text">
                <span class="display-2">{{{ $stretch_count }}}</span>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card text-white bg-danger mb-3">
            <div class="card-header">weights</div>
            <div class="card-body">
              @foreach ($lifting_list as $item)
              <div class="entry">
                  <h5 class="card-title">Reps</h5>
                  <p class="card-text">
                    <span class="display-2">{{{ $item->reps }}}</span>
                  </p>
                  <h5 class="card-title">Weight</h5>
                  <p class="card-text">
                    <span class="display-2">{{{ $item->weight }}}</span>lbs
                  </p>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
      </div>
      <!-- End of Cards -->
      <div class="tab-pane fade" id="v-pills-add" role="tabpanel" aria-labelledby="v-pills-add-tab">
        <!-- Form -->
        <p class="lead">
          Add activity
        </p>
        <hr>
        <form method="POST" action="{{ route('exercises.store') }}">
          @csrf
          <div class="form-group">
            <label for="activity">Activity</label>
            <input type="text" class="form-control" id="activity" name="activity">
          </div>
          <div class="form-group">
            <label for="type">Select type</label>
            <select class="form-control" id="type" name="type">
              <option>Cardio</option>
              <option>Weight-lifting</option>
              <option>Stretch</option>
            </select>
          </div>
          <div class="sub-title">Cardio & Stretch Only</div>
          <hr>
          <!-- Duration for Cardio & Stretch -->
          <div class="form-group">
            <label for="duration">Duration (in min)</label>
            <input type="text" class="form-control" id="duration" name="duration">
          </div>
          <div class="sub-title">Weight-Lifting Only</div>
          <hr>
          <!-- Reps -->
          <div class="form-group">
            <label for="reps"># of Reps</label>
            <input type="text" class="form-control" id="reps" name="reps">
          </div>
          <!-- Weight -->
          <div class="form-group">
            <label for="weight">Weight (in lbs)</label>
            <input type="text" class="form-control" id="weight" name="weight">
          </div>
          <button type="submit" class="btn btn-danger">Add</button>
        </form>
        <!-- End Form -->
      </div>
    </div>
  </div>
</div>
@stop