# Calorie-free health tracker
This Laravel app enables the user to enter and track the following:
* Water, Fruit, and Vegetable intake
* Cardio, Stretch, and Weight-lifting activities
* Stress Relief activities like meditation, spending time with family and friends, hobbies, free time, etc.

The idea behind the app is to provide users with a way to track their health goals that isn't overly focused on granular metrics (i.e. calories). It is backed by a sqlite database and uses bootstrap.

# Getting Started

Go to https://bodyandmindtracker.herokuapp.com.

On the home page, you can read a more about the app.

![Home Page](./public/images/home.png)

The summary page provides an overview of the user's activity. Individual sections (Diet, Exercise, Stress Relief) break these down into smaller sections and enable the user to add new entries.

![Summary Page](./public/images/summary.png)

All entries can be found on the *All* section of the summary page.

![All Section](./public/images/all.png)

Users can also add goals with a deadline.

![Goals Page](./public/images/goals.png)

# In the future

1. Currently the app includes create and read functionality. It will soon have update and delete functionality as well.

2. User accounts so that individual users can log in and see just their data.